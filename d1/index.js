let posts = [],count = 1;

document.querySelector('#form-add-post').addEventListener('submit', (e) => {
    e.preventDefault();

    posts.push({
        id: count,
        title: document.querySelector('#txt-title').value,
        body: document.querySelector('#txt-body').value
    });
    count ++;
    showPosts(posts);
    // alert('Successfully Added');
});

const showPosts = (posts) => {
    let postEntries ="";
    posts.forEach((post) => {
        postEntries += `
            <div id = "post-${post.id}">
                <h3 id="post-title-${post.id}">${post.title}</h3>
                <p id="post-body-${post.id}">${post.body}</p>
                <button onclick="editPost(${post.id})">Edit</button>
                <button onclick="deletePost(${post.id})">Delete</button>
            </div>
        `
    });
    document.querySelector('#div-post-entries').innerHTML = postEntries;
};

const editPost = (val) => {
    document.querySelector('#txt-edit-id').value = val;
    document.querySelector('#txt-edit-title').value = document.querySelector(`#post-title-${val}`).innerHTML;
    document.querySelector('#txt-edit-body').value = document.querySelector(`#post-body-${val}`).innerHTML;
}

document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
    e.preventDefault();

    let index = posts.findIndex(x => x.id.toString() === document.querySelector('#txt-edit-id').value);
    console.log(index); 
    if(index > -1) {
        posts[index].title = document.querySelector('#txt-edit-title').value;
        posts[index].body = document.querySelector('#txt-edit-body').value;
        showPosts(posts);
        alert("Successfully updated.")
    }
});

const deletePost = (val) => {
    let parentElem = document.querySelector('#div-post-entries');
    parentElem.removeChild(document.querySelector(`#post-${val}`));
   let index = posts.findIndex(x => x.id.toString() == val);
   if(index > -1) {
        posts.splice(index,1);
   }
}